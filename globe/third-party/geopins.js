function loadGeoData(latlonData, left, onePlanet) {

    var rad = 200;

    //	can we even find the country in the list?
    // if( countryLookup[country.n.toUpperCase()] === undefined ){
    // 	console.log('could not find country that has country code: ' + country.n)
    // 	continue;
    // }

    //	save out country name and code info

    //	take the lat lon from the data and convert this to 3d globe space

    var phi = (90 - latlonData.lat) * Math.PI / 180;
    var theta = (180 - latlonData.lon) * Math.PI / 180;

    var center = new THREE.Vector3();
    center.x = rad * Math.sin(phi) * Math.cos(theta) + (onePlanet ? 0 : (left ? -250 : +250));
    center.y = rad * Math.cos(phi);
    center.z = rad * Math.sin(phi) * Math.sin(theta);
    //
    //var lon = 90 - latlonData.lon;
    //var lat = 180 - latlonData.lat;
    //
    //var phi = Math.PI / 2 - lat * Math.PI / 180 - Math.PI * 0.01;
    //var theta = 2 * Math.PI - lon * Math.PI / 180 + Math.PI * 0.06;
    //
    //var center = new THREE.Vector3();
    //center.x = Math.sin(phi) * Math.cos(theta) * rad + (left ? -250 : +250);
    //center.y = Math.cos(phi) * rad;
    //center.z = Math.sin(phi) * Math.sin(theta) * rad;

    //	save and catalogue
    var country = new Object();
    country.center = center;
    return country;
}

//	convenience function to get the country object by name
function getCountry(name) {
    return countryData[name.toUpperCase()]
}