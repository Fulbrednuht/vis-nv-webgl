/**
 * Created by anton on 11/09/14.
 */
var debug = true;
var mysql = require('mysql'),
    http = require("http"),
    url = require("url"),
    q = require('querystring');
var type, sqlRequest;

// Datenbankverbindung
var mysqlConnectionPool = mysql.createPool({
    connectionLimit: 10,
    host: '10.3.1.205',
    //host: 'localhost',
    user: 'vish',
    password: 'vish2014',
    database: 'vishPcap',
    port: 3306
});


/**
 * buildSqlRequest diese Methode parst das http request nach parametern
 * und baut daraus das entsprechende SQL statment.
 */
function buildSqlRequest(httpQuery) {
    var insert = [];
    var date = httpQuery["date"];
    var time = httpQuery["time"];
    var protocol = httpQuery["protocol"];
    var port = httpQuery["port"];
    var all = httpQuery["all"];

    type = httpQuery["type"];

    var sqlRequestHours, sqlRequestDays, sqlRequestConnections, sqlRequestConnectionsSrc, sqlRequestExtra;

    if (typeof(date) == 'undefined') {
        sqlRequest = null;
        return sqlRequest
    }

    // Umwandeln des Datums von 20141123 nach 2014-11-23)
    if (type == 'hour') {
        if (typeof(time) == 'undefined') time = 0;
        insert[0] = date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6) + " " + time + ":00";
        insert[1] = date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6) + " " + time + ":59";
    }
    else{
        insert[0] = date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6);
        insert[1] = date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6) + " 23:59";
    }
    // 1 Planet -Anzahl Pakete- stundenweise
    sqlRequestHours = "select longitude, latitude, log(packages / 5) packages, date " +
    " from cache_geoData_hourly " +
    " where latitude not like '52.4' AND longitude not like '9.7'" +
    " AND latitude not like '0.0' AND longitude not like '0.0'";
    if(all == undefined)
        sqlRequestHours += " AND date BETWEEN ? AND ?";

    // 1 Planet -Anzahl Pakete- tageweise
    sqlRequestDays = "select longitude, latitude, log(sum(packages) / 5) packages, date(date) date from cache_geoData_hourly  " +
    " where latitude not like '52.4' AND longitude not like '9.7' " +
    " AND latitude not like '0.0' AND longitude not like '0.0'";
    if(all == undefined)
        sqlRequestDays += " AND date BETWEEN ? AND ?";
    sqlRequestDays += " group by date(date), longitude, latitude";

    // 1 Planet -Verbindungen- aus Hannover
    sqlRequestConnectionsSrc = "select srcLong, srcLat, dstLong, dstLat, date(date) date, srcIP, dstIP, log(sum(packages) / 5) packages" +
    " from cache_connectionData" +
    " where srcIP like '10.2.%' ";
    if(all == undefined)
        sqlRequestConnectionsSrc += " AND date BETWEEN ? AND ? ";
    sqlRequestConnectionsSrc += " AND srcLong not like '52.4' AND srcLat not like '9.7' AND dstLong not like '52.4' AND dstLat not like '9.7' " +
    " AND srcLong not like '0.0' AND srcLat not like '0.0' AND dstLong not like '0.0' AND dstLat not like '0.0' " +
    " group by date(date), srcLong, srcLat, dstLong, dstLat";

    // 2 Planeten -Verbindungen- tageweise
    sqlRequestConnections = "select srcLong, srcLat, dstLong, dstLat, date(date) date, srcIP, dstIP, log(sum(packages) / 5) packages" +
    " from cache_connectionData" +
    " where  srcLong not like '0.0' AND srcLat not like '0.0' AND dstLong not like '0.0' AND dstLat not like '0.0' ";
    if(all == undefined)
        sqlRequestConnections += " AND date BETWEEN ? AND ?";
    sqlRequestConnections += " group by date(date), srcLong, srcLat, dstLong, dstLat";

    if (type == 'day') {
        sqlRequest = sqlRequestDays;
    }
    else if (type == 'hour') {
        sqlRequest = sqlRequestHours;
    }
    else if (type == 'conn') {
        sqlRequest = sqlRequestConnections;
    }
    else if (type == 'src') {
        sqlRequest = sqlRequestConnectionsSrc;
    }
    else {
        sqlRequest = null;
        return sqlRequest
    }

    /*if (typeof(protocol) != 'undefined' || typeof(port) != 'undefined') {
        if (type == 'hour') {
            sqlRequestExtra = "select longitude, latitude, date, sum(packageCount) as packages " +
            " from ConnectionData c join geoIPData g on (g.ip=c.dstIP)" +
            " where latitude not like '52.4' AND longitude not like '9.7'" +
            " AND date BETWEEN ? AND ?";

            if (typeof(protocol) != 'undefined') {
                sqlRequestExtra += " AND protocol = ? ";
                insert[insert.length] = protocol;
            }
            if (typeof(port) != 'undefined') {
                sqlRequestExtra += " AND dstPort = ? ";
                insert[insert.length] = port;
            }
            sqlRequestExtra += " group by date, longitude, latitude; "
        }
        if (type == 'day') {
            sqlRequestExtra = "select longitude, latitude, date(date) date, sum(packageCount) as packages" +
            " from (select longitude, latitude, date, sum(packageCount) as packages " +
            " from ConnectionData c join geoIPData g on (g.ip=c.dstIP)" +
            " where latitude not like '52.4' AND longitude not like '9.7'" +
            " AND date BETWEEN ? AND ?";

            if (typeof(protocol) != 'undefined') {
                sqlRequestExtra += " AND protocol = ? ";
                insert[insert.length] = protocol;
            }
            if (typeof(port) != 'undefined') {
                sqlRequestExtra += " AND dstPort = ? ";
                insert[insert.length] = port;
            }
            sqlRequestExtra += " group by date, longitude, latitude) sub " +
            "group by date(date), latitude, longitude; "
        }
        sqlRequest = sqlRequestExtra;
    }*/
    if (debug) console.log(sqlRequest);
    if (debug) console.log(insert);

    sqlRequest = mysql.format(sqlRequest, insert);
    return sqlRequest;
}

/**
 * Erzeugung eines HTTP-Servers
 */
http.createServer(function (req, resp) {

    if (req.method = 'GET') {
        httpQuery = q.parse(req.url);
        var strQuery = buildSqlRequest(httpQuery);               // SQL Statement erzeugen
        if (strQuery != null) {
            getDataFromMysql(strQuery, function (err, sqlResp) { // DB Abfrage

                if (sqlResp != null) {                           // Abfrage erfolgreich
                    resp.writeHead(200, {
                        'Content-Type': 'x-application/json',
                        'Access-Control-Allow-Origin': '*'
                    });
                    resp.end(sqlResp);
                    if (debug) console.log(sqlRequest);
                    if (err != null) console.log(err);
                }
                else {                                          // Abfrage fehlgeschlagen
                    resp.writeHead(400, {'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
                    resp.end("MYSQL -> " + err);
                    if (debug) {
                        console.log(sqlRequest);
                    }
                }
            });
        } else {
            resp.writeHead(400, {'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*'});
            resp.end("Falsche Anfrage! \nErlaubte Anfrage: \n10.3.1.205:8888/&type=hour&date=20141121");
        }
    }
    else {
        //Fehlerfall (POST, PUT, DELETE)
        if (debug) console.log(sqlRequest);
    }
    req.resume();
}).listen(8888, "10.3.1.205");

/**
 * Funktion die die DB-Abfrage aus
 * @param strQuery SQL Statement
 * @param callback Callback zum Zurückliefern der Ergebnisse
 */
function getDataFromMysql(strQuery, callback) {
    var json = '';
    mysqlConnectionPool.query(strQuery, function (err, rows) {
        if (err) {
            //throw err;
            return callback(err, null);
        } else {
            json = clamping(rows);
            callback(null, json);
        }
    });
}

/**
 * Clampen des PackageCount für einfachere Verwendung der Daten
 * @returns geclampte Daten
 */
function clamping(data) {
    var min = 999999;
    var max = 0;

    // min/max suchen
    for (var i = 0; i < data.length; i++) {
        if (data[i].packages < min) {
            min = data[i].packages;
        }
        if (data[i].packages > max) {
            max = data[i].packages;
        }
    }

    if (debug == true) {
        console.log("min: " + min + "max: " + max);
    }
    if (max == 0) {
        return null;
    }
    //clampen der Daten
    for (var i = 0; i < data.length; i++) {
        var pack = parseFloat(data[i].packages);
        data[i].packages = (pack - min) / max;
    }

    return JSON.stringify(data);
}



