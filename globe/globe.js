/**
 * dat.globe Javascript WebGL Globe Toolkit
 * http://dataarts.github.com/dat.globe
 *
 * Copyright 2011 Data Arts Team, Google Creative Lab
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

var DAT = DAT || {};
var onePlanet, withLines;

DAT.Globe = function (container, opts) {
    opts = opts || {};
    onePlanet = (opts.onePlanet == undefined) ? true : opts.onePlanet;
    withLines = (opts.withLines == undefined) ? false : opts.withLines;
    var colorFn = opts.colorFn || function (x) {
            var c = new THREE.Color();
            c.setHSL(( 0.6 - ( x * 0.5 ) ), 1.0, 0.5);
            return c;
        };
    var imgDir = opts.imgDir || ''; // Die oder Variable von '/globe/' in '' geändert


    var Shaders = {
        'earth': {
            uniforms: {
                'texture': {type: 't', value: null}
            },
            vertexShader: [
                'varying vec3 vNormal;',
                'varying vec2 vUv;',
                'void main() {',
                'gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
                'vNormal = normalize( normalMatrix * normal );',
                'vUv = uv;',
                '}'
            ].join('\n'),
            fragmentShader: [
                'uniform sampler2D texture;',
                'varying vec3 vNormal;',
                'varying vec2 vUv;',
                'void main() {',
                'vec3 diffuse = texture2D( texture, vUv ).xyz;',
                'float intensity = 1.05 - dot( vNormal, vec3( 0.0, 0.0, 1.0 ) );',
                'vec3 atmosphere = vec3( 1.0, 1.0, 1.0 ) * pow( intensity, 3.0 );',
                'gl_FragColor = vec4( diffuse + atmosphere, 1.0 );',
                '}'
            ].join('\n')
        },
        'atmosphere': {
            uniforms: {},
            vertexShader: [
                'varying vec3 vNormal;',
                'void main() {',
                'vNormal = normalize( normalMatrix * normal );',
                'gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );',
                '}'
            ].join('\n'),
            fragmentShader: [
                'varying vec3 vNormal;',
                'void main() {',
                'float intensity = pow( 0.8 - dot( vNormal, vec3( 0, 0, 1.0 ) ), 12.0 );',
                'gl_FragColor = vec4( 1.0, 1.0, 1.0, 1.0 ) * intensity;',
                '}'
            ].join('\n')
        }
    };

    var camera, scene, renderer, w, h;
    var leftHalf = false;
    var meshPlanet, meshAtmo, atmosphere, point, meshPoints, meshPlanet2, meshAtmo2, meshPoints2;

    var overRenderer;

    var curZoomSpeed = 0;
    var zoomSpeed = 50;

    var mouse = {x: 0, y: 0}, mouseOnDown = {x: 0, y: 0};
    var rotation = {x: 0, y: 0},
        target = {x: Math.PI * 3 / 2, y: Math.PI / 6.0},
        targetOnDown = {x: 0, y: 0};

    var rotation2 = {x: 0, y: 0},
        target2 = {x: Math.PI * 3 / 2, y: Math.PI / 6.0},
        targetOnDown2 = {x: 0, y: 0};

    var distance = 100000, distanceTarget = 100000;
    var padding = 40;
    var PI_HALF = Math.PI / 2;
    var visualizationMesh;
    var lineGeoms = [];

    function init() {

        container.style.color = '#fff';
        container.style.font = '13px/20px Arial, sans-serif';

        var shader, uniforms, material;
        w = container.offsetWidth || window.innerWidth;
        h = container.offsetHeight || window.innerHeight;

        camera = new THREE.PerspectiveCamera(30, w / h, 1, 10000);
        camera.position.z = distance;

        scene = new THREE.Scene();

        var geometry = new THREE.SphereGeometry(200, 40, 30);

        shader = Shaders['earth'];
        uniforms = THREE.UniformsUtils.clone(shader.uniforms);

        //uniforms['texture'].value = THREE.ImageUtils.loadTexture(imgDir+'world.jpg');
        //uniforms['texture'].value = THREE.ImageUtils.loadTexture(imgDir+'world_darkBlue.jpg');
        uniforms['texture'].value = THREE.ImageUtils.loadTexture(imgDir + 'world_borders.jpg');
        material = new THREE.ShaderMaterial({

            uniforms: uniforms,
            vertexShader: shader.vertexShader,
            fragmentShader: shader.fragmentShader

        });

        meshPlanet = new THREE.Mesh(geometry, material);
        meshPlanet.rotation.y = Math.PI;
        if(!onePlanet) {
            meshPlanet.position.x = -250;
        }
        scene.add(meshPlanet);

        meshPlanet2 = new THREE.Mesh(geometry, material);
        meshPlanet2.rotation.y = Math.PI;
        meshPlanet2.position.x = 250;
        if(!onePlanet) {
            scene.add(meshPlanet2);
        }

        shader = Shaders['atmosphere'];
        uniforms = THREE.UniformsUtils.clone(shader.uniforms);

        material = new THREE.ShaderMaterial({

            uniforms: uniforms,
            vertexShader: shader.vertexShader,
            fragmentShader: shader.fragmentShader,
            side: THREE.BackSide,
            blending: THREE.AdditiveBlending,
            transparent: true

        });

        meshAtmo = new THREE.Mesh(geometry, material);
        meshAtmo.scale.set(1.1, 1.1, 1.1);
        if(!onePlanet) {
            meshAtmo.position.x = -268;
        }
        scene.add(meshAtmo);

        meshAtmo2 = new THREE.Mesh(geometry, material);
        meshAtmo2.scale.set(1.1, 1.1, 1.1);
        meshAtmo2.position.x = 268;
        if(!onePlanet) {
            scene.add(meshAtmo2);
        }

        geometry = new THREE.CubeGeometry(0.75, 0.75, 1);
        geometry.applyMatrix(new THREE.Matrix4().makeTranslation(0, 0, -0.5));

        point = new THREE.Mesh(geometry);

        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setSize(w, h);

        renderer.domElement.style.position = 'absolute';

        container.appendChild(renderer.domElement);

        container.addEventListener('mousedown', onMouseDown, false);

        container.addEventListener('mousewheel', onMouseWheel, false);

        document.addEventListener('keydown', onDocumentKeyDown, false);

        window.addEventListener('resize', onWindowResize, false);

        container.addEventListener('mouseover', function () {
            overRenderer = true;
        }, false);

        container.addEventListener('mouseout', function () {
            overRenderer = false;
        }, false);
    }

    addData = function (data, opts) {

        cancelAnimationFrame(animate);
        if(this._baseGeometry !== undefined) {
            this._baseGeometry.dispose();
            delete this._baseGeometry;
            this._baseGeometry = null;
        }
        var lat, lng, size, color, i, step, colorFnWrapper;

        opts.animated = opts.animated || false;
        this.is_animated = opts.animated;
        opts.format = opts.format || 'magnitude'; // other option is 'legend'
        console.log(opts.format);
        if (opts.format === 'magnitude') {
            step = 3;
            colorFnWrapper = function (data, i) {
                return colorFn(data[i + 2]);
            }
        } else if (opts.format === 'legend') {
            step = 4;
            colorFnWrapper = function (data, i) {
                return colorFn(data[i + 3]);
            }
        } else {
            throw('error: format not supported: ' + opts.format);
        }

        if (opts.animated) {
            if (this._baseGeometry === undefined) {
                this._baseGeometry = new THREE.Geometry();
                if(!onePlanet) {
                    meshPlanet.position.x += 250;
                }
                for (i = 0; i < data.length; i += step) {
                    lat = data[i];
                    lng = data[i + 1];
                    size = data[i + 2] * 30 + 0.9;
                    color = colorFnWrapper(data, i);
                    //       size = 0;
                    addPoint(lat, lng, size, color, this._baseGeometry);
                }
                if(!onePlanet) {
                    meshPlanet.position.x -= 250;
                }
            }
            if (this._morphTargetId === undefined) {
                this._morphTargetId = 0;
            } else {
                this._morphTargetId += 1;
            }
            opts.name = opts.name || 'morphTarget' + this._morphTargetId;
        }
        this._baseGeometry = null;
        var subgeo = new THREE.Geometry();
        if(!onePlanet) {
            meshPlanet.position.x += 250;
        }
        for (i = 0; i < data.length; i += step) {
            lat = data[i];
            lng = data[i + 1];
            color = colorFnWrapper(data, i);
            size = data[i + 2];
            size = size * 120;
            addPoint(lat, lng, size, color, subgeo);
        }
        if(!onePlanet) {
            meshPlanet.position.x -= 250;
        }
        if (opts.animated) {
            this._baseGeometry.morphTargets.push({'name': opts.name, vertices: subgeo.vertices});
        } else {
            this._baseGeometry = subgeo;
        }

    };

    function createPoints() {
        if(meshPoints !== undefined) {
            scene.remove(meshPoints);
            meshPoints = null
        }
        if(meshPoints2 !== undefined) {
            scene.remove(meshPoints2);
            meshPoints2 = null
        }
        if (this._baseGeometry !== undefined) {
            if (this.is_animated === false) {
                meshPoints = new THREE.Mesh(this._baseGeometry, new THREE.MeshBasicMaterial({
                    color: 0xffffff,
                    vertexColors: THREE.FaceColors,
                    morphTargets: false
                }));
                meshPoints2 = new THREE.Mesh(this._baseGeometry, new THREE.MeshBasicMaterial({
                    color: 0xffffff,
                    vertexColors: THREE.FaceColors,
                    morphTargets: false
                }));
            } else {
                if (this._baseGeometry.morphTargets.length < 8) {
                    console.log('t l', this._baseGeometry.morphTargets.length);
                    var padding = 8 - this._baseGeometry.morphTargets.length;
                    console.log('padding', padding);
                    for (var i = 0; i <= padding; i++) {
                        console.log('padding', i);
                        this._baseGeometry.morphTargets.push({
                            'name': 'morphPadding' + i,
                            vertices: this._baseGeometry.vertices
                        });
                    }
                }
                meshPoints = new THREE.Mesh(this._baseGeometry, new THREE.MeshBasicMaterial({
                    color: 0xffffff,
                    vertexColors: THREE.FaceColors,
                    morphTargets: true
                }));
                meshPoints2 = new THREE.Mesh(this._baseGeometry, new THREE.MeshBasicMaterial({
                    color: 0xffffff,
                    vertexColors: THREE.FaceColors,
                    morphTargets: true
                }));
            }
            meshPoints2.position.x = 250;
            if(!onePlanet) {
                meshPoints.position.x = -250;
                scene.add(meshPoints2);
            }
            scene.add(meshPoints);
        }
    }

    addLine = function (data) {

        cancelAnimationFrame(animate);
        var i = 0;
        var step = 5;
        if(visualizationMesh !== undefined) {
            scene.remove(visualizationMesh);
            visualizationMesh = null;
            if(lineGeoms !== undefined) {
                while(lineGeoms.length == 0) {
                    var linegeo = lineGeoms.pop();
                    linegeo.dispose();
                    delete lineGeo;
                }
                delete lineGeoms;
                lindeGeoms = [];
            }
        }

        visualizationMesh = new THREE.Object3D();

        for(i = 0; i < data.length; i += step) {
            var longlat = new Object();
            longlat.lat = data[i];
            longlat.lon = data[i + 1];
            var longlat2 = new Object();
            longlat2.lat = data[i + 2];
            longlat2.lon = data[i + 3];
            var start = loadGeoData(longlat, true, onePlanet);
            var end = loadGeoData(longlat2, false, onePlanet);
            var color = colorFn(data[i + 4]);
            var lineWidth = data[i + 4] + 5;
            var curveGeometry = makeConnectionLineGeometry(start, end, 10, true, onePlanet);
            lineGeoms.push(curveGeometry);

            var splineOutline = new THREE.Line(curveGeometry, new THREE.LineBasicMaterial(
                    {
                        color: color, opacity: 1.0, blending: THREE.AdditiveBlending, transparent: true,
                        depthWrite: false,
                        linewidth: lineWidth
                    })
            );
            visualizationMesh.add(splineOutline);
        }

        //splineOutline.renderDepth = false;
        //visualizationMesh.add(splineOutline);
        scene.add(visualizationMesh);
    };

    function addPoint(lat, lng, size, color, subgeo) {

        var phi = (90 - lat) * Math.PI / 180;
        var theta = (180 - lng) * Math.PI / 180;

        point.position.x = 200 * Math.sin(phi) * Math.cos(theta);
        point.position.y = 200 * Math.cos(phi);
        point.position.z = 200 * Math.sin(phi) * Math.sin(theta);

        point.lookAt(meshPlanet.position);

        point.scale.z = Math.max(size, 0.1); // avoid non-invertible matrix
        point.updateMatrix();

        for (var i = 0; i < point.geometry.faces.length; i++) {

            point.geometry.faces[i].color = color;

        }

        THREE.GeometryUtils.merge(subgeo, point);
    }

    function onMouseDown(event) {
        event.preventDefault();

        container.addEventListener('mousemove', onMouseMove, false);
        container.addEventListener('mouseup', onMouseUp, false);
        container.addEventListener('mouseout', onMouseOut, false);

        mouseOnDown.x = -event.clientX;
        mouseOnDown.y = event.clientY;

        var widthhalf = container.offsetWidth / 2;
        if(event.x < widthhalf) {
            leftHalf = true;
        } else {
            leftHalf = false;
        }

        if(leftHalf || onePlanet) {
            targetOnDown.x = target.x;
            targetOnDown.y = target.y;
        } else {
            targetOnDown2.x = target2.x;
            targetOnDown2.y = target2.y;
        }
        container.style.cursor = 'move';
    }

    function onMouseMove(event) {
        mouse.x = -event.clientX;
        mouse.y = event.clientY;

        var zoomDamp = distance / 1000;
        if(leftHalf || onePlanet) {
            target.x = targetOnDown.x + (mouse.x - mouseOnDown.x) * 0.005 * zoomDamp;
            target.y = targetOnDown.y + (mouse.y - mouseOnDown.y) * 0.005 * zoomDamp;

            target.y = target.y > PI_HALF ? PI_HALF : target.y;
            target.y = target.y < -PI_HALF ? -PI_HALF : target.y;
        } else {
            target2.x = targetOnDown2.x + (mouse.x - mouseOnDown.x) * 0.005 * zoomDamp;
            target2.y = targetOnDown2.y + (mouse.y - mouseOnDown.y) * 0.005 * zoomDamp;

            target2.y = target2.y > PI_HALF ? PI_HALF : target2.y;
            target2.y = target2.y < -PI_HALF ? -PI_HALF : target2.y;
        }
    }

    function onMouseUp(event) {
        container.removeEventListener('mousemove', onMouseMove, false);
        container.removeEventListener('mouseup', onMouseUp, false);
        container.removeEventListener('mouseout', onMouseOut, false);
        container.style.cursor = 'auto';
    }

    function onMouseOut(event) {
        container.removeEventListener('mousemove', onMouseMove, false);
        container.removeEventListener('mouseup', onMouseUp, false);
        container.removeEventListener('mouseout', onMouseOut, false);
    }

    function onMouseWheel(event) {
        event.preventDefault();
        if (overRenderer) {
            zoom(event.wheelDeltaY * 0.3);
        }
        return false;
    }

    function onDocumentKeyDown(event) {
        switch (event.keyCode) {
            case 38:
                zoom(100);
                event.preventDefault();
                break;
            case 40:
                zoom(-100);
                event.preventDefault();
                break;
        }
    }

    function onWindowResize(event) {
        camera.aspect = container.offsetWidth / container.offsetHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(container.offsetWidth, container.offsetHeight);
    }

    function zoom(delta) {
        distanceTarget -= delta;
        distanceTarget = distanceTarget > 1350 ? 1350 : distanceTarget;
        distanceTarget = distanceTarget < 280 ? 280 : distanceTarget;
    }

    function animate() {
        requestAnimationFrame(animate);
        render();
    }

    function render() {
        zoom(curZoomSpeed);

        rotation.x += (target.x - rotation.x) * 0.1;
        rotation.y += (target.y - rotation.y) * 0.1;
        rotation2.x += (target2.x - rotation2.x) * 0.1;
        rotation2.y += (target2.y - rotation2.y) * 0.1;
        distance += (distanceTarget - distance) * 0.3;

        meshPlanet.rotation.y = -rotation.x + Math.PI;
        meshPlanet.rotation.x = rotation.y;

        meshPlanet2.rotation.y = -rotation2.x + Math.PI;
        meshPlanet2.rotation.x = rotation2.y;

        meshAtmo.rotation.y = -rotation.x;
        meshAtmo.rotation.x = rotation.y;

        meshAtmo2.rotation.y = -rotation2.x;
        meshAtmo2.rotation.x = rotation2.y;

        if(meshPoints !== undefined) {
            meshPoints.rotation.y = -rotation.x;
            meshPoints.rotation.x = rotation.y;
        }

        if(meshPoints !== undefined) {
            meshPoints2.rotation.y = -rotation.x;
            meshPoints2.rotation.x = rotation.y;
        }
        // meshPlanet.position.z = Math.cos(rotation.x) * Math.cos(rotation.y);

        camera.position.z = distance;

        camera.lookAt(new THREE.Vector3(0, 0, 0));
        if (visualizationMesh !== undefined) {

            visualizationMesh.traverse(
                function (geom) {
                    if (geom.geometry !== undefined) {
                        if (geom.geometry.update !== undefined) {
                            if(onePlanet) {
                                geom.geometry.update(meshAtmo.rotation, meshAtmo.rotation);
                            } else {
                                geom.geometry.update(meshAtmo.rotation, meshAtmo2.rotation);
                            }
                        }
                    }
                }
            );
        }

        renderer.render(scene, camera);
    }

    init();
    this.animate = animate;


    this.__defineGetter__('time', function () {
        return this._time || 0;
    });

    this.__defineSetter__('time', function (t) {
        var validMorphs = [];
        var morphDict = meshPoints.morphTargetDictionary;
        for (var k in morphDict) {
            if (k.indexOf('morphPadding') < 0) {
                validMorphs.push(morphDict[k]);
            }
        }
        validMorphs.sort();
        var l = validMorphs.length - 1;
        var scaledt = t * l + 1;
        var index = Math.floor(scaledt);
        for (i = 0; i < validMorphs.length; i++) {
            meshPoints.morphTargetInfluences[validMorphs[i]] = 0;
        }
        var lastIndex = index - 1;
        var leftover = scaledt - index;
        if (lastIndex >= 0) {
            meshPoints.morphTargetInfluences[lastIndex] = 1 - leftover;
        }
        meshPoints.morphTargetInfluences[index] = leftover;
        this._time = t;
    });

    function empty(elem) {
        while (elem.lastChild) elem.removeChild(elem.lastChild);
    }

    this.addData = addData;
    this.createPoints = createPoints;
    this.renderer = renderer;
    this.scene = scene;
    this.addLine = addLine;

    return this;

};

