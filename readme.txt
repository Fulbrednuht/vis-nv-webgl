project title:  Externer Netzwerkverkehr der Hochschule

team members:   Patrick Gabriel - Anton Saenko - Jan Worthmann

source of data: sim-extern.inform.fh-hannover.de:8888/ - NodeJS Server, nur aus dem FH-Netz erreichbar!! Parameter für Abfragen type=[hour, day, src, conn] date=[datum vom 1.11.2014 bis 21.12.2014]
                sim-extern.inform.fh-hannover.de:8888/&type=hour&date=20141104 - Beispielabfrage


source of code: ...
JavaScript libraries, examples, ... are used:
    https://github.com/dataarts/webgl-globe  - Beispiel Projekt von Google - verwendet Three als Scenegraph und nutzt WebGL
    http://jquery.com/                       - Ein-/Ausblenden der GUI-Elemeten, Timeslider
    http://nodejs.org/                       - Netzwerk JavaScript Bibliothek - HTTP-Schnittstelle zur Datenbank


brief description of usage:
Öffnen der Seite über HTTP Server. Der Server sollte im vis-nv-webgl verzeichnis gestartet werden. Dann einfach globe/index.html, einfach im Browser öffnen.
Als Browser sollte Chrome verwendet werden, in anderen Browsern kann es Probleme mit dem Zoom und der Planetensteuerung geben.

Auf der linken Seite kann unter Select das Datum für die Netzwerkdaten gewählt werden. Das Format muss dabei YYYYMMDD sein. (z.B. 20141101)
Auf der rechten Seite kann man unter Views, vier verschiedene Ansichten für die Daten wählen.

View 1: Zeigt wohin wie viele Pakete gesendet wurden, stundenweise aggregiert. Zur verbesseren Darstellenung wurden die Daten aus dem internen Netz herrausgefiltert
        Auf der linken Seite kann die Uhrzeit ausgewählt werden. Über den play Button kann eine fortlaufende Animation über die Stunden gestartet werden.

View 2: Zeigt wohin wie viele Pakete gesendet wurden, tageweise aggregiert. Zur verbesseren Darstellenung wurden die Daten aus dem internen Netz herrausgefiltert

View 3: Zeigt die ausgehende Verbindungen von der Hochschule aus, die in Welt gehen. Tageweise aggregiert.

View 4: Zeigt alle aufgezeichneten Verbindungen zwischen zwei Planeten an. Tageweise aggregiert.


brief list of implemented visualization techniques:
- Color Mapping
-- HSL-Farbmodell
- Rasterung der Geo-Daten
- Visualisierung von Graphkanten
-