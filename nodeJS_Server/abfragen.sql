-- Packete pro Stunde und long/lat
select longitude, latitude, date, sum(packageCount) as packages 
from ConnectionData c join geoIPData g on (g.ip=c.dstIP) 
where date BETWEEN '2014-11-01' AND '2014-11-01 23:59' 
group by date, longitude, latitude; 

-- Verbindungsdaten
select gs.longitude srcLong, gs.latitude srcLat, gd.longitude dstLong, gd.latitude dstLat, date, srcIP, dstIP, sum(packageCount)
from ConnectionData c join geoIPData gs on (gs.ip=c.srcIP) 
		      						join geoIPData gd on (gd.ip=c.dstIP)
where date BETWEEN '2014-11-01' AND '2014-11-01 23:59' 
group by date, gs.longitude, gs.latitude, gd.longitude, gd.latitude;

-- Verbindungsdaten mit Ursprung aus Hannover
select gs.longitude srcLong, gs.latitude srcLat, gd.longitude dstLong, gd.latitude dstLat, date, srcIP, dstIP
from ConnectionData c join geoIPData gs on (gs.ip=c.srcIP) 
		      						join geoIPData gd on (gd.ip=c.dstIP)
where date BETWEEN '2014-11-01' AND '2014-11-01 23:59' 
      AND srcIP like '10.2.%'
group by date, longitude, latitude;

-- create cache_connectionData
create table cache_connectionData 
select gs.longitude srcLong, gs.latitude srcLat, gd.longitude dstLong, gd.latitude dstLat, date, srcIP, dstIP, sum(packageCount) packages
from ConnectionData c 	join geoIPData gs on (gs.ip=c.srcIP) 
												join geoIPData gd on (gd.ip=c.dstIP)
group by date, gs.longitude, gs.latitude, gd.longitude, gd.latitude;


-- create cache_geoData_hourly 
create table cache_geoData_hourly 
select longitude, latitude, date, sum(packageCount) as packages 
from ConnectionData c join geoIPData g on (g.ip=c.dstIP)  
group by date, longitude, latitude; 

