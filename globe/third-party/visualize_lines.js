var globeRadius = 1000;
var vec3_origin = new THREE.Vector3(0,0,0);

function makeConnectionLineGeometry( exporter, importer, value, type, onePlanet ){

	console.log("test");
	var distanceBetweenCountryCenter = exporter.center.clone().sub(importer.center).length();

	//	start of the line
	var start = exporter.center;

	//	end of the line
	var end = importer.center;
	
	//	midpoint for the curve
	var mid = start.clone().lerp(end,0.5);
	var midLength = mid.length()
	mid.normalize();
	var mult = (onePlanet) ? 0.7 : 0.2;
	mid.multiplyScalar( midLength + distanceBetweenCountryCenter * mult );

	//	the normal from start to end
	var normal = (new THREE.Vector3()).sub(start,end);
	normal.normalize();

	/*				     
				The curve looks like this:
				
				midStartAnchor---- mid ----- midEndAnchor
			  /											  \
			 /											   \
			/												\
	start/anchor 										 end/anchor

		splineCurveA							splineCurveB
	*/

	var distanceHalf = distanceBetweenCountryCenter * 0.5;

	var startAnchor = start;
	var midStartAnchor = mid.clone().add( normal.clone().multiplyScalar( distanceHalf ) );
	var midEndAnchor = mid.clone().add( normal.clone().multiplyScalar( -distanceHalf ) );
	var endAnchor = end;

	//	now make a bezier curve out of the above like so in the diagram
	var splineCurveA = new THREE.CubicBezierCurve3( start, startAnchor, midStartAnchor, mid);											
	// splineCurveA.updateArcLengths();

	var splineCurveB = new THREE.CubicBezierCurve3( mid, midEndAnchor, endAnchor, end);
	// splineCurveB.updateArcLengths();

	//	how many vertices do we want on this guy? this is for *each* side
	var vertexCountDesired = Math.floor( /*splineCurveA.getLength()*/ distanceBetweenCountryCenter * 0.02 + 6 ) * 2;	

	//	collect the vertices
	var points = splineCurveA.getPoints( vertexCountDesired );

	//	remove the very last point since it will be duplicated on the next half of the curve
	points = points.splice(0,points.length-1);

	points = points.concat( splineCurveB.getPoints( vertexCountDesired ) );

	//	add one final point to the center of the earth
	//	we need this for drawing multiple arcs, but piled into one geometry buffer
	// points.push( vec3_origin );

	var val = value * 0.0003;
	
	var size = (10 + Math.sqrt(val));
	size = constrain(size,0.1, 60);

	//	create a line geometry out of these
	var curve = new THREE.SplineCurve3( points );
	var curveGeometry = new THREE.Geometry();

	curveGeometry.start = exporter.center;
	curveGeometry.end = importer.center;
	curveGeometry.vertices = curve.getPoints(50);

	curveGeometry.size = size;

	curveGeometry.actualRotationS = new THREE.Euler( 0, 0, 0, 'XYZ' );
	curveGeometry.actualRotationE = new THREE.Euler( 0, 0, 0, 'XYZ' );
		curveGeometry.update = function (newRotationS, newRotationE) {
			if (!this.actualRotationS.equals(newRotationS) || !this.actualRotationE.equals(newRotationE)) {
				this.actualRotationS = newRotationS.clone();
				this.actualRotationE = newRotationE.clone();
				var newStart = this.start.clone();
				var newEnd = this.end.clone();
				if(!onePlanet) {
					newStart.x += 250;
					newEnd.x -= 250;
				}
				newStart.applyEuler(newRotationS);
				newEnd.applyEuler(newRotationE);
				if(!onePlanet) {
					newStart.x -= 250;
					newEnd.x += 250;
				}
				var distanceBetweenCountryCenter = newStart.clone().sub(newEnd).length();

				//	midpoint for the curve
				var mid = newStart.clone().lerp(newEnd, 0.5);
				var midLength = mid.length()
				mid.normalize();
				var mult = (onePlanet) ? 0.7 : 0.2;
				mid.multiplyScalar(midLength + distanceBetweenCountryCenter * mult);

				//	the normal from start to end
				var normal = (new THREE.Vector3()).subVectors(newStart, newEnd);
				normal.normalize();

				var distanceHalf = distanceBetweenCountryCenter * 0.5;

				var startAnchor = newStart;
				var midStartAnchor = mid.clone().add(normal.clone().multiplyScalar(distanceHalf));
				var midEndAnchor = mid.clone().add(normal.clone().multiplyScalar(-distanceHalf));
				var endAnchor = newEnd;

				//	now make a bezier curve out of the above like so in the diagram
				var splineCurveA = new THREE.CubicBezierCurve3(newStart, startAnchor, midStartAnchor, mid);
				// splineCurveA.updateArcLengths();

				var splineCurveB = new THREE.CubicBezierCurve3(mid, midEndAnchor, endAnchor, newEnd);
				// splineCurveB.updateArcLengths();

				//	how many vertices do we want on this guy? this is for *each* side
				var vertexCountDesired = Math.floor(/*splineCurveA.getLength()*/ distanceBetweenCountryCenter * 0.02 + 6) * 2;

				//	collect the vertices
				var points = splineCurveA.getPoints(vertexCountDesired);

				//	remove the very last point since it will be duplicated on the next half of the curve
				points = points.splice(0, points.length - 1);

				points = points.concat(splineCurveB.getPoints(vertexCountDesired));

				//	add one final point to the center of the earth
				//	we need this for drawing multiple arcs, but piled into one geometry buffer
				// points.push( vec3_origin );

				var val = value * 0.0003;

				var size = (10 + Math.sqrt(val));
				size = constrain(size, 0.1, 60);

				//	create a line geometry out of these
				var curve = new THREE.SplineCurve3(points);
				curveGeometry.vertices = curve.getPoints(50);
				this.verticesNeedUpdate = true;
			}
		}


	return curveGeometry;
}

function constrain(v, min, max){
	if( v < min )
		v = min;
	else
	if( v > max )
		v = max;
	return v;
}